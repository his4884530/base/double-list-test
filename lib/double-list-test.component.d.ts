import { EventEmitter, OnInit } from '@angular/core';
import { CodeName, DoubleSet, Group } from './doubleset.interface';
import * as i0 from "@angular/core";
export declare class DoubleListTestComponent implements OnInit {
    value: DoubleSet;
    item: CodeName;
    itemChange: EventEmitter<CodeName>;
    currentGroup?: Group;
    ngOnInit(): void;
    onGroupClick(groupName: string): void;
    onItemClick(item: CodeName): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DoubleListTestComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DoubleListTestComponent, "lib-double-list-test", never, { "value": { "alias": "value"; "required": false; }; "item": { "alias": "item"; "required": false; }; }, { "itemChange": "itemChange"; }, never, never, true, never>;
}
