import * as i0 from '@angular/core';
import { Injectable, EventEmitter, Component, Input, Output } from '@angular/core';
import { NgFor, NgClass } from '@angular/common';
import * as i1 from 'primeng/splitter';
import { SplitterModule } from 'primeng/splitter';
import * as i2 from 'primeng/api';

class DoubleListTestService {
    constructor() { }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "16.1.8", ngImport: i0, type: DoubleListTestService, deps: [], target: i0.ɵɵFactoryTarget.Injectable }); }
    static { this.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "16.1.8", ngImport: i0, type: DoubleListTestService, providedIn: 'root' }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "16.1.8", ngImport: i0, type: DoubleListTestService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class DoubleListTestComponent {
    constructor() {
        this.value = {};
        this.item = {};
        this.itemChange = new EventEmitter();
    }
    ngOnInit() {
        this.currentGroup = this.value.groups[0];
    }
    onGroupClick(groupName) {
        this.currentGroup = this.value.groups.find(x => x.name === groupName);
    }
    onItemClick(item) {
        this.itemChange.emit(item);
    }
    static { this.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "16.1.8", ngImport: i0, type: DoubleListTestComponent, deps: [], target: i0.ɵɵFactoryTarget.Component }); }
    static { this.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "16.1.8", type: DoubleListTestComponent, isStandalone: true, selector: "lib-double-list-test", inputs: { value: "value", item: "item" }, outputs: { itemChange: "itemChange" }, ngImport: i0, template: "<div>\n  <p-splitter styleClass=\"mb-5\">\n    <ng-template pTemplate>\n      <div class=\"group\">\n        <h3>{{ value.title }}</h3>\n        <ul>\n          <button *ngFor=\"let group of value.groups\" (click)=\"onGroupClick(group.name)\"\n            [ngClass]=\"currentGroup?.name === group.name ? 'selected' : 'normal'\">\n            {{ group.name }}\n          </button>\n        </ul>\n      </div>\n    </ng-template>\n\n    <ng-template pTemplate>\n      <div class=\"item\">\n        <h3>{{ value.subTitle }}</h3>\n        <ul>\n          <button *ngFor=\"let currentItem of currentGroup?.items\" (click)=\"onItemClick(currentItem)\"\n            [ngClass]=\"item.code === currentItem.code ? 'selected' : 'normal'\">\n            {{ currentItem.name }}\n            <div class=\"content\"> {{ currentItem.code }} : {{ currentItem.name }}</div>\n          </button>\n        </ul>\n      </div>\n    </ng-template>\n  </p-splitter>\n</div>\n", styles: ["::ng-deep .p-splitter{border:none;background:#ffffff;border-radius:6px}ul,button{margin:0%;padding:0%;list-style:none}ul{display:flex;flex-direction:row;flex-wrap:wrap}button{box-shadow:0 2px 1px #6f797940,0 -1px 1px 1px #6f79791a;width:5rem;text-align:center;margin-top:10px;margin-right:5px;margin-bottom:10px;border:transparent;position:relative;background-color:transparent;cursor:pointer}.content{display:none}button:hover .content{display:block;position:absolute;top:-30px;width:auto;right:0;color:#fff;font-size:.8em;background:#346e7a;padding:5px;border-radius:5px}.group,.item{padding-left:.5rem}.selected{background-color:#fdd3a4}.normal{background-color:transparent}\n"], dependencies: [{ kind: "directive", type: NgFor, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "ngmodule", type: SplitterModule }, { kind: "component", type: i1.Splitter, selector: "p-splitter", inputs: ["styleClass", "panelStyleClass", "style", "panelStyle", "stateStorage", "stateKey", "layout", "gutterSize", "step", "minSizes", "panelSizes"], outputs: ["onResizeEnd", "onResizeStart"] }, { kind: "directive", type: i2.PrimeTemplate, selector: "[pTemplate]", inputs: ["type", "pTemplate"] }] }); }
}
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "16.1.8", ngImport: i0, type: DoubleListTestComponent, decorators: [{
            type: Component,
            args: [{ selector: 'lib-double-list-test', standalone: true, imports: [NgFor, NgClass, SplitterModule], template: "<div>\n  <p-splitter styleClass=\"mb-5\">\n    <ng-template pTemplate>\n      <div class=\"group\">\n        <h3>{{ value.title }}</h3>\n        <ul>\n          <button *ngFor=\"let group of value.groups\" (click)=\"onGroupClick(group.name)\"\n            [ngClass]=\"currentGroup?.name === group.name ? 'selected' : 'normal'\">\n            {{ group.name }}\n          </button>\n        </ul>\n      </div>\n    </ng-template>\n\n    <ng-template pTemplate>\n      <div class=\"item\">\n        <h3>{{ value.subTitle }}</h3>\n        <ul>\n          <button *ngFor=\"let currentItem of currentGroup?.items\" (click)=\"onItemClick(currentItem)\"\n            [ngClass]=\"item.code === currentItem.code ? 'selected' : 'normal'\">\n            {{ currentItem.name }}\n            <div class=\"content\"> {{ currentItem.code }} : {{ currentItem.name }}</div>\n          </button>\n        </ul>\n      </div>\n    </ng-template>\n  </p-splitter>\n</div>\n", styles: ["::ng-deep .p-splitter{border:none;background:#ffffff;border-radius:6px}ul,button{margin:0%;padding:0%;list-style:none}ul{display:flex;flex-direction:row;flex-wrap:wrap}button{box-shadow:0 2px 1px #6f797940,0 -1px 1px 1px #6f79791a;width:5rem;text-align:center;margin-top:10px;margin-right:5px;margin-bottom:10px;border:transparent;position:relative;background-color:transparent;cursor:pointer}.content{display:none}button:hover .content{display:block;position:absolute;top:-30px;width:auto;right:0;color:#fff;font-size:.8em;background:#346e7a;padding:5px;border-radius:5px}.group,.item{padding-left:.5rem}.selected{background-color:#fdd3a4}.normal{background-color:transparent}\n"] }]
        }], propDecorators: { value: [{
                type: Input
            }], item: [{
                type: Input
            }], itemChange: [{
                type: Output
            }] } });

/*
 * Public API Surface of double-list-test
 */

/**
 * Generated bundle index. Do not edit.
 */

export { DoubleListTestComponent, DoubleListTestService };
//# sourceMappingURL=his-double-list-test.mjs.map
